﻿using System;
using System.IO;
using System.Xml.Xsl;

namespace xmlToxml
{
  class Program
  {
    static void Main(string[] args)
    {
      string workingDirectory = Environment.CurrentDirectory;
      string baseName = workingDirectory + @"\files\";
      XslTransform xslTransform = new XslTransform();
      xslTransform.Load(baseName + "transform.xslt");
      xslTransform.Transform(baseName + "from.xml", baseName + "to.xml");
      Console.WriteLine("Done, Press any key...");
      Console.ReadKey();
    }
  }
}
