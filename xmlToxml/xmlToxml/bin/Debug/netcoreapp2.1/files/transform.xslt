<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">  
        <OrderDetails>
			<xsl:for-each select="catalog/cd">
				<Item>
					<xsl:value-of select="title"/>
				</Item>
			</xsl:for-each>
        </OrderDetails>
		<Fotter>
			<Currency>
				<xsl:value-of select="concat(catalog/cd1/price/currency/amount, ' ',catalog/cd1/price/currency/type)"/>
			</Currency>
		</Fotter>
    </xsl:template>
</xsl:stylesheet>